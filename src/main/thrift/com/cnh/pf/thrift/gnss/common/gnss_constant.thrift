/*
 * Copyright (C) 2015 CNH Industrial NV. All rights reserved.
 *
 * This software contains proprietary information of CNH Industrial NV. Neither
 * receipt nor possession thereof confers any right to reproduce, use, or
 * disclose in whole or in part any such information without written
 * authorization from CNH Industrial NV.
 *
 */

/**
 * file copied from gnssthrift.git project for refactoring
 **/
 
include "model/gnss_model.thrift"

namespace java com.cnh.pf.thrift.gnss.common

/** Socket ports */
const i32 PCM_PORT = 17220;

/** Common constants */
const i32 UNDEFINED = -1;

const i32 POSITION_QUALITY_FAVOR_ACCURACY = 0;
const i32 POSITION_QUALITY_BALANCED_QUALITY = 1;
const i32 POSITION_QUALITY_FAVOR_AVAILABILITY = 2;

/** XFill constants */
const i32 XFILL_RUNTIME_DEFAULT = 20; // minutes

/** XFill Base Datum constants */
const i32 WGS84_ITRF2008 = 0;
const i32 NAD83 = 1;
const i32 ETRS89 = 2;
const i32 GDA94 = 3;

/** XFill Frequency constants */
const i32 XFILL_FREQUENCY_TYPE_WESTERN_NORTH_AMERICA = 0;
const i32 XFILL_FREQUENCY_TYPE_CENTRAL_NORTH_AMERICA = 1;
const i32 XFILL_FREQUENCY_TYPE_EASTERN_NORTH_AMERICA = 2;
const i32 XFILL_FREQUENCY_TYPE_LATIN_AMERICA = 3;
const i32 XFILL_FREQUENCY_TYPE_EUROP_AFRICA = 4;
const i32 XFILL_FREQUENCY_TYPE_ASIA_PACIFIC = 5;
const i32 XFILL_FREQUENCY_TYPE_CUSTOM = 6;

/** Data Parity constants */
const i32 DATA_PARITY_ODD = 0;
const i32 DATA_PARITY_EVEN = 1;
const i32 DATA_PARITY_NONE = 2;

/** Protocol constants */
const i32 PROTOCOL_CMR = 0;
const i32 PROTOCOL_RTCM3 = 1;

/** QUESTION: Does particular antenna vertical offset is a constant? So we can keep these constants here */
const gnss_model.Antenna ANTENNA_AG_25 = {'antennaModel': gnss_model.AntennaModel.AG_25, 'verticalOffsetCm': 7.23};
const gnss_model.Antenna ANTENNA_ZEPHYR_II = {'antennaModel': gnss_model.AntennaModel.ZEPHYR_II, 'verticalOffsetCm': 8.43};
const gnss_model.Antenna ANTENNA_ZEPHYR_II_RUGGED = {'antennaModel': gnss_model.AntennaModel.ZEPHYR_II_RUGGED, 'verticalOffsetCm': 9.73};

// declaration of this list produces 'type error: const "ANTENNAS<elem>" was declared as struct/xception'
//const list<gnss_model.Antenna> ANTENNAS = [ANTENNA_AG_25, ANTENNA_ZEPHYR_II, ANTENNA_ZEPHYR_II_RUGGED];
