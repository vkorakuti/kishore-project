/*
 * Copyright (C) 2015 CNH Industrial NV. All rights reserved.
 *
 * This software contains proprietary information of CNH Industrial NV. Neither
 * receipt nor possession thereof confers any right to reproduce, use, or
 * disclose in whole or in part any such information without written
 * authorization from CNH Industrial NV.
 *
 */

/**
 * file copied from gnssthrift.git project for refactoring
 **/

namespace java com.cnh.pf.thrift.gnss.common.model


/**
* Enums and struct from trimble receiver, some of them will be removed
**/

/**
 * Enumaration of all known correction sources
 **/
enum CorrectionSource {
   // DECLARATION ORDER IS IMPORTANT (?) (for test java server I use enum position as id)
   AUTONOMOUS = 1,
   CENTER_POINT_RTK = 2,
   CENTER_POINT_VRS = 3,
   CENTER_POINT_RTX_SATELLITE_FAST_CONVERGENCE = 4,
   CENTER_POINT_RTX_MODEM_FAST_CONVERGENCE = 5,
   CENTER_POINT_RTX_SATELLITE_STANDARD_CONVERGENCE = 6,
   CENTER_POINT_RTX_MODEM_STANDARD_CONVERGENCE = 7,
   OMNISTAR_HP_G2 = 8, // Omni Star-Omni star HP/G2
   OMNISTAR_VBS = 9,
   RANGE_POINT_RTX = 10,
   DGPS_SATELLITE_SBAS = 11,
   DGPS_RADIO_RTCM = 12,
   DGPS_MODEM_RTCM = 13,
}

/**
 * GPS Satellite Type, GNSS Manager Satellite Types are zero-based, match with 
 * Packet by subtracting 1 from index.
 **/
enum SatType {
   GPS = 1,
   SBAS = 2,
   GLONASS = 3,
   GALILEO = 4,
   QZSS = 5,
   BEIDOU_2COMPASS = 6,
}

/**
 * Port, on which the gnss-receiver is connected
 **/
enum ConnectionPort {
   CAN_BUS_1,
   CAN_BUS_2,
   CAN_BUS_3,
   CAN_BUS_4,
   RS232,
   CAN_OVER_IP
}

/** 
* SBAS Correction Type Subtypes
**/
enum SBASType {
   WAAS,
   EGNOS,
   GAGAN,
   SDCM,
   MSAS,
   UNKNOWN,
}

/**
 * SBAS Corrections Source details
 **/
struct SbasDetail {
   1: i32 numSats;
   2: double snr;
   3: double hdop;
   4: double pdop;
   5: i32 prn;
   6: SBASType sbasType;
} 

/**
* Represents information about GPS receiver
**/
struct GnssReceiver {
   1: ConnectionPort connectionPort;
   2: string brand;
   3: string model;
   4: string serialNumber
   5: bool hasAntennas = false;
}

/**
* Represents enumeration of all known antenna models, third party antennas will not be listed.
**/
enum AntennaModel {
   AG_25,
   ZEPHYR_II,
   ZEPHYR_II_RUGGED,
   CUSTOM, // custom antenna (user configured)
}

/**
* Represents info about antenna
*
* TODO: user provided string for 3rd party antennas?
**/
struct Antenna {
   1: AntennaModel antennaModel; // model probably should be an ENUM instead of string
   2: double verticalOffsetCm; //cm
}

/**
* Enumaration of radio types for 'Center Point RTK' and 'Center Point VRS' correction sources
**/
enum RadioType {
   /** Center Point RTK **/
   INTERNAL,
   /** Center Point RTK **/
   EXTERNAL,
   /** Center Point VRS **/
   DCM300,
   /** Center Point VRS **/
   THIRD_PARTY,
   /** thrift **/
   NONE,
}

/**
* List of avaliable satellite health modes
**/
enum SatelliteHealth {
   ON,
   OFF,
   HEED_HEALTH,
   IGNORE_HEALTH,
}

/**
* Represents correction satellite objecs. It should probably have GPS receiver S/N as they may be different from one receiver to another.
**/
struct CorrectionSatellite {
   1: i32 id;
   2: string name;
   3: SatelliteHealth satelliteHealth;
   /** Is it the same satellite data that we use for GNSS configureation and GPS UI in Window Manager? **/
   4: i32 snr;
   5: i32 elevation;
   6: i32 azimuth;
   7: double latitude;
   8: double longitude;
}

/**
* Represents security key objects
**/
struct SecurityKey {
   1: string key;
   2: string note;
   /** timestamp (instead of long (i64) timestamp value we can use encoded string i.e. http://en.wikipedia.org/wiki/ISO_8601 not sure what is better for us right now) **/
   3: i64 expirationDate;
}

/**
* List of config properties
**/
enum Property {
   /** Internal Radio. **/
   PROPERTY_NETWORK_ID,
   /** Internal Radio. **/
   SCINTILLATION_MODE,
   /** Internal Radio. **/
   BACKUP_CORRECTION_SOURCE,

   /** External Radio **/
   BAUD_RATE,
   /** External Radio **/
   DATA_PARITY,
   /** External Radio **/
   STOP_BITS,
   /** External Radio **/
   PROTOCOL,

   /** XFill **/
   XFILL_RUNTIME,
   /** XFill **/
   XFILL_BASE_DATUM,
   /** XFill **/
   XFILL_FREQUENCY_TYPE,
   /** XFill **/
   XFILL_FREQUENCY,
   /** XFill **/
   XFILL_BAUD_RATE,

   /** DCM300 **/
   USER_NAME,
   /** DCM300 **/
   PASSWORD,
   /** DCM300 **/
   VRS_SERVER_NAME,
   /** DCM300 **/
   VRS_SERVER_PORT,
   /** DCM300 **/
   MOUNT_POINT,
   /** DCM300 **/
   POSITION_QUALITY,

   /** RTX **/
   FREQUENCY,
   /** RTX **/
   CONVERGENCE_THRESHOLD,
   /** RTX **/
   FAST_RESTART,
   /** RTX MODEM **/
   DEVICE,

   /** Center Point RTK may be 900Mhz or 450Mhz, this property should return one of these values **/
   RTK_RADIO_FREQUENCY,
}

/**
* Represents GPS location with all related data
**/
struct Location {
   1: double latitude;
   2: double longitude;
   3: double altitude;
   4: double bearing;
   5: double speed;
   /**
   * timestamp - days since January 1, 1970, relative to UTC time
   * **/
   6: i64 time;
   7: double hdop;
   8: double pdop;
   9: double age;
}

/**
* Enumeration of all possible correction statuses
**/
enum CorrectionStatus {
   FIX,
   FLOAT,
   CONVERGED,
   NOT_CONVERGED,
   NO_SIGNAL,
}

/**
* Represents a set of runtime data for 'Center Point RTK' correction source
**/
struct CenterPointRTKCorrectionSourceInfo {
   1: i32 numberOfSatellites;
   2: double hdop;
   3: CorrectionStatus correctionStatus;
   4: i32 networkId;
   5: double ionoScinitillation;
   6: double correctionAge;
   /** suppose to be 900Mhz or 450Mhz **/
   7: i32 frequency;
}

/**
* Represents a set of runtime data for 'Omni Star' correction source
**/
struct OmniStarCorrectionSourceInfo {
   1: i32 numberOfSatellites;
   2: double hdop;
   3: CorrectionStatus correctionStatus;
   4: i32 networkId;
   5: double correctionAge;
   6: double convergeLevel;
   7: double signalLevel;
}

/**
* Represents a set of runtime data for 'Center Point VRS' correction source
**/
struct CenterPointVRSCorrectionSourceInfo {
   1: i32 numberOfSatellites;
   2: double hdop;
   3: double vdop;
   4: double ionoScinitillation;
   5: double cellSignalStrength;
   6: CorrectionStatus correctionStatus;
   7: double correctionAge;
}

/**
* Represents a set of runtime data for 'DGPS SBAS' correction source
**/
struct DGPSSBASCorrectionSourceInfo {
   1: i32 numberOfSatellites;
   2: double hdop;
   4: double correctionAge;
   5: i32 networkId;
   6: double signalLevel;
   7: SBASType sbasType;
}

/**
* Represents a set of runtime data for 'RTX Satellite (Fast Convergence)' correction source
**/
struct RTXSatelliteFastCorrectionSourceInfo {
   1: i32 numberOfSatellites;
   2: double hdop;
   3: CorrectionStatus correctionStatus;
   4: double correctionAge;
   5: i32 networkId;
   6: double vdop;
}

/**
* Represents a set of runtime data for 'RTX Satellite (Standard Convergence)' correction source
**/
struct RTXSatelliteStandardCorrectionSourceInfo {
   1: i32 numberOfSatellites;
   2: double hdop;
   3: CorrectionStatus correctionStatus;
   4: double correctionAge;
   5: i32 networkId;
   6: double vdop;
   7: double signalLevel;
   8: double convergeLevel;
}

/**
* Represents a set of runtime data for 'RTX Modem (Standard Convergence)' correction source
**/
struct RTXModemStandardCorrectionSourceInfo {
   1: i32 numberOfSatellites;
   2: double hdop;
   3: CorrectionStatus correctionStatus;
   4: double cellSignalStrength;
   5: double vdop;
   6: double convergeLevel;
}

/**
* Represents a set of runtime data for 'RTX Modem (Standard Convergence)' correction source
**/
struct RangePointRTXCorrectionSourceInfo {
   // To be defined
}

/**
* Wrapper around correction source info of different types
**/
struct CorrectionSourceInfo {
   1: CorrectionSource correctionSource;
   2: optional CenterPointRTKCorrectionSourceInfo centerPointRTKCorrectionSourceInfo;
   3: optional OmniStarCorrectionSourceInfo omniStarCorrectionSourceInfo;
   4: optional CenterPointVRSCorrectionSourceInfo centerPointVRSCorrectionSourceInfo;
   5: optional DGPSSBASCorrectionSourceInfo dgpsSBASCorrectionSourceInfo;
   6: optional RTXSatelliteFastCorrectionSourceInfo rtxSatelliteFastCorrectionSourceInfo;
   7: optional RTXSatelliteStandardCorrectionSourceInfo rtxXSatelliteStandardCorrectionSourceInfo;
   8: optional RTXModemStandardCorrectionSourceInfo rtxXModemStandardCorrectionSourceInfo;
   9: optional RangePointRTXCorrectionSourceInfo rangePointRTXCorrectionSourceInfo;
}

/**
* Data Structure for Satellites-In-View
**/
struct SatelliteData {
   1: i32 prn;
   2: SatType satType;
   3: double elevationDegress;
   4: double azimuthDegress;
   5: double l1BandSnrDB;
   6: double l2BandSnrDB;
   7: bool inSolution;
}


/**
* Structures and enums for the new CNHi GNSS Receiver for Next Generation Guidance
**/

enum SatelliteSystem {
   GPS = 0,
   GLONASS = 1,
   SBAS = 2,
   GALILEO = 5,
   BEIDOU = 6,
   QZSS = 7,
}

/**
 * L-Band Assignment Option
 */
enum LBandAssignmentOption {
   IDLE = 0,
   AUTO = 1,
   MANUAL = 2,
}

/**
* A satellite that is currently in view by the receiver
* Filled by SATVIS2 log from Novatel receiver
**/
struct SatelliteInView {
   1: SatelliteSystem system;
   2: i32 prn;    //slot for glonass, prn for other systems (GPS, sbas)
   3: i32 glonassFrequencyChannel;  //frequency channel for glonass, zero for other systems
   4: i32 satelliteHealth;
   5: double elevationDegrees;
   6: double azimuthDegrees;
   7: double theoreticalDoppler;
   8: double apparentDoppler;
}

/**
* All visibible satellites for a particular satellite system
* Filled by SATVIS2 log from Novatel receiver
**/
struct VisibleSatellites {
   1: SatelliteSystem system;
   2: bool visibilityValid;
   3: bool completeAlmanacUsed;
   4: i32 numberOfSatellites;
   5: list<SatelliteInView> satellites;
}

/**
* Satellite Observation Status
**/
enum SatelliteObservationStatus {
   GOOD = 0, //Observation is good
   BADHEALTH = 1, //Satellite is flagged as bad health in ephemeris or almanac
   OLDEPHEMERIS = 2, //Ephemeris >3 hours old
   ELEVATIONERROR = 6, //Satellite was below the elevation cutoff
   MISCLOSURE = 7, //Observation was too far from predicted value
   NODIFFCORR = 8, //No differential correction available
   NOEPHEMERIS = 9, //No ephemeris available
   INVALIDIODE = 10, //IODE used is invalid
   LOCKEDOUT = 11, //Satellite has been locked out
   LOWPOWER = 12, //Satellite has low signal power
   OBSL2 = 13, //An L2 observation not directly used in the solution
   UNKNOWN = 15, //Observation was not used because it was of an unknown type
   NOIONOCORR = 16, //No ionosphere delay correction was available
   NOTUSED = 17, // Observation was not used in the solution
   OBSL1 = 18, //An L1 observation not directly used in the solution
   OBSE1 = 19, //An E1 observation not directly used in the solution
   OBSL5 = 20, //An L5 observation not directly used in the solution
   OBSE5 = 21, //An E5 observation not directly used in the solution
   OBSB2 = 22, //A B2 observation not directly used in the solution
   OBSB1 = 23, //A B1 observation not directly used in the solution
   NOSIGNALMATCH = 25, //Signal type does not match
   SUPPLEMENTARY = 26, //Observation contributes supplemental information to the solution
   NA = 99, //No observation available
   BAD_INTEGRITY = 100, //Observation was an outlier and was eliminated from the solution
   LOSSOFLOCK = 101, //Lock was broken on this signal
   NOAMBIGUITY = 102 //No RTK ambiguity type resolved
}

/**
* Satellite used in BESTPOS or RTKPOS
* filled by BESTSATS and RTKSATS logs from the receiver
**/
struct SatelliteUsed {
   1: SatelliteSystem system;
   2: i32 prn;    //slot for glonass, prn for other systems (GPS, sbas)
   3: i32 glonassFrequencyChannel;  //frequency channel for glonass, zero for other systems
   4: SatelliteObservationStatus satelliteStatus;
   5: i32 signalMask;
}

/**
* System Used for Timing
**/
enum TimingSystem {
   GPS = 0,
   GLONASS = 1,
   GALILEO = 2,
   BEIDOU = 3,
   AUTO = 99   //AUTO is used only as a backup system
}

/**
* TDOP values (Time DOP)
**/
struct TDopValue {
   1: TimingSystem system;
   2: double timeDop;
}

/**
* DOP values
* filled by PSRDOP2 and RTKDOP2 logs from receiver
**/
struct DopValues{
   1: double geometricDop;
   2: double positionDop;
   3: double horizontalDop;
   4: double verticalDop;
   5: list<TDopValue> timeDopValues;
}

/**
* L-Band beam
**/
struct LBandBeam {
   1: string name;   //Beam/transmitting satellite name
   2: string regionID;
   3: i32 frequency;
   4: i32 baudRate;
   5: double longitude; //Transmitting satellite longitude (degrees)
}

/**
* L-Band tracking status
**/
struct LBandTrackingStatus {
   1: string name; //Beam/transmitting satellite name
   2: i32 frequency; //Frequency assigned to this L-Band beam (Hz)
   3: i32 baudRate; //Baud rate of assigned beam
   4: i32 serviceID; //Service ID of the assigned beam
   5: i16 trackingStatus; //Tracking status word
   6: double doppler; //signal doppler (Hz)
   7: double carrierNoiseDensityRatio; //Carrier to noise density ratio (dB-Hz)
   8: double phaseErrorStandardDeviation; //Phase error standard deviation (cycles)
   9: double lockTime; //seconds
   10: i32 totalUniqueWordBits;
   11: i32 badUniqueWordBits;
   12: i32 badUniqueWords;
   13: i32 totalViterbiSymbols;
   14: i32 correctedViterbiSymbols;
   15: double estimatedPreViterbiErrorRate;
}

/**
* DGPS Types
**/
enum DgpsType {
   RTCM = 0,
   RTCA = 1,
   CMR = 2,
   OMNISTAR = 3,
   SBAS = 5,
   RTK = 6,
   AUTO = 10,
   NONE = 11,
   RTCMV3 = 13,
   NOVATELX = 14
}

/**
* Port on which correction is output.
* used by the following commands: INTERFACEMODE, GENERATEALIGNCORRECTIONS, RTKPORTMODE
**/
enum CorrectionOutputPort {
    CCOM1 = 38,
    CCOM2 = 39,
    CCOM3 = 40,
    CCOM4 = 41,
    CCOM5 = 42
}
