/*
 * Copyright (C) 2015 CNH Industrial NV. All rights reserved.
 *
 * This software contains proprietary information of CNH Industrial NV. Neither
 * receipt nor possession thereof confers any right to reproduce, use, or
 * disclose in whole or in part any such information without written
 * authorization from CNH Industrial NV.
 *
 */

/**
 * file copied from gnssthrift.git project for refactoring
 **/
 
namespace java com.cnh.pf.thrift.gnss.common.exception

/**
* Indicates that radio type is illegal for specific correction source
**/
exception IllegalRadioTypeException {
   1: string message;
}

/**
* Indicates that property doesn't exists
**/
exception PropertyDoesNotExistException {
   1: string message;
}

/**
* Indicates that property exists but it is not the same type as requested
**/
exception IncorrectPropertyTypeException {
   1: string message;
}
