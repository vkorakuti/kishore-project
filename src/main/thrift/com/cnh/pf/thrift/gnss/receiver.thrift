/*
 * Copyright (C) 2015 CNH Industrial NV. All rights reserved.
 *
 * This software contains proprietary information of CNH Industrial NV. Neither
 * receipt nor possession thereof confers any right to reproduce, use, or
 * disclose in whole or in part any such information without written
 * authorization from CNH Industrial NV.
 *
 */

namespace java com.cnh.pf.thrift.gnss.receiver

/**
* Enums and structs used in binary commands and responses for the CNHi GNSS Receiver for NGG
*
* put in thrift for easier maintenance
*
* for descriptions see CNHi GNSS Receiver ICD for NGG and Novatel OEM6 Family Firmware Reference Manual
**/


/**
* Novatel receiver command and log ids
**/
enum Command {
   UNKNOWN = 0,
   LOG = 1,
   INTERFACEMODE = 3,
   COM = 4,
   CLOCKADJUST = 15,
   RESET = 18,
   SAVECONFIG = 19,
   FRESET = 20,
   MODEL = 22,
   CLOCKSTEERING = 26,
   ASSIGN = 27,
   ASSIGNALL = 28,
   UNASSIGN = 29,
   UNASSIGNALL = 30,
   UNLOG = 36,
   VERSION = 37,
   UNLOGALL = 38,
   BESTPOS = 42,
   FIX = 44,
   PSRPOS = 47,
   AUTH = 49,
   ECUTOFF = 50,
   USERDATUM = 78,
   TRACKSTAT = 83,
   RTKELEVMASK = 91,
   RTKSVENTRIES = 92,
   STATUSCONFIG = 95,
   MATCHEDPOS = 96,
   RTKCOMMAND = 97,
   ANTENNAPOWER = 98,
   TIME = 101,
   SETAPPROXTIME = 102,
   RXCONFIG = 128,
   SETRTCM16 = 131,
   LOCKOUT = 137,
   UNLOCKOUT = 138,
   UNLOCKOUTALL = 139,
   RTKPOS = 141,
   DGPSEPHEMDELAY = 142,
   DGPSTXID = 144,
   LOGFILE = 157,
   DATUM = 160,
   SETNAV = 162,
   POSAVE = 173,
   SEND = 177,
   SENDHEX = 178,
   MAGVAR = 180,
   RTKDYNAMICS = 183,
   NVMRESTORE = 197,
   VALIDMODELS = 206,
   SOFTPOWER = 213,
   UNDULATION = 214,
   GPGGA = 218,
   GPGSA = 221,
   GPGST = 222,
   GPGSV = 223,
   GPRMC = 225,
   GPVTG = 226,
   GPZDA = 227,
   EXTERNALCLOCK = 230,
   FREQUENCYOUT = 232,
   PASSCOM1 = 233,
   PASSCOM2 = 234,
   DYNAMICS = 258,
   SETAPPROXPOS = 377,
   INSCOMMAND = 379,
   APPLICATION = 413,
   PDPFILTER = 424,
   ADJUST1PPS = 429,
   CLOCKCALIBRATE = 430,
   COMCONTROL = 431,
   PDPPOS = 469,
   SOFTLOADCOMMIT = 475,
   SOFTLOADRESET = 476,
   SOFTLOADSREC = 477,
   PSRDIFFSOURCE = 493,
   RTKSOURCE = 494,
   CLOCKOFFSET = 596,
   POSTIMEOUT = 612,
   PPSCONTROL = 613,
   MARKCONTROL = 614,
   SBASCONTROL = 652,
   SETDIFFCODEBIASES = 687,
   GGAQUALITY = 691,
   SETIONOTYPE = 711,
   ASSIGNLBAND = 729,
   GLOECUTOFF = 735,
   UTMZONE = 749,
   FIXPOSDATUM = 761,
   MOVINGBASESTATION = 763,
   COMVOUT = 779,
   HPSTATICINIT = 780,
   HPSEED = 782,
   USEREXPDATUM = 783,
   FORCEGPSL2CODE = 796,
   SETBESTPOSCRITERIA = 839,
   RTKQUALITYLEVEL = 844,
   CNOUPDATE = 849,
   RTKANTENNA = 858,
   NMEATALKER = 861,
   BASEANTENNAMODEL = 870,
   LOCKOUTSYSTEM = 871,
   SETRTCM36 = 880,
   UNLOCKOUTSYSTEM = 908,
   RTKTIMEOUT = 910,
   DIFFCODEBIASCONTROL = 913,
   LOCALIZEDCORRECTION_DATUM = 947,
   RTKNETWORK = 951,
   TUNNELESCAPE = 962,
   PDPMODE = 970,
   RAWSBASFRAME = 973,
   SBASECUTOFF = 1000,
   SBASTIMEOUT = 1001,
   DLLTIMECONST = 1011,
   SATVIS2 = 1043,
   HDTOUTTHRESHOLD = 1062,
   HEADINGOFFSET = 1082,
   SETCANNAME = 1091,
   GALECUTOFF = 1114,
   SETROVERID = 1135,
   CHANCONFIGLIST = 1148,
   SELECTCHANGCONFIG = 1149,
   SETUTCLEAPSECONDS = 1150,
   PSRDOP2 = 1163,
   RTKDOP2 = 1172,
   RTKSATS = 1174,
   MATCHEDSATS = 1176,
   BESTSATS = 1194,
   OMNIUSEGLONASS = 1199,
   ASSIGNLBAND2 = 1200,
   LBANDTRACKSTAT = 1201,
   IONOCONDITION = 1215,
   SETRTCMRXVERSION = 1216,
   FORCEGLOL2CODE = 1217,
   SOFTLOADDATA = 1218,
   SOFTLOADSETUP = 1219,
   SETTIMEBASE = 1237,
   IPCONFIG = 1243,
   DNSCONFIG = 1244,
   ETHCONFIG = 1245,
   SERIALCONFIG = 1246,
   ECHO = 1247,
   ICOMCONFIG = 1248,
   NTRIPCONFIG = 1249,
   GENERATERTKCORRECTIONS = 1260,
   RAIMMODE = 1285,
   GENERATEDIFFCORRECTIONS = 1296,
   SOFTLOADFILE = 1302,
   SETRTCMTXVERSION = 1322,
   ALIGNAUTOMATION = 1323,
   PDPVELOCITYOUT = 1324,
   TRACKSV = 1326,
   NTRIPSOURCETABLE = 1343,
   AUTHCODES = 1348,
   GENERATEALIGNCORRECTIONS = 1349,
   QZSSECUTOFF = 1350,
   DOSCMD = 1355,
   SETBASERECEIVERTYPE = 1374,
   PROFILE = 1411,
   BASEANTENNAPCO = 1415,
   BASEANTENNAPCV = 1416,
   THISANTENNAPCO = 1417,
   THISANTENNAPCV = 1418,
   BASEANTENNATYPE = 1419,
   THISANTENNATYPE = 1420,
   SBASALMANAC = 1425,
   BESTGNSSPOS = 1429,
   SETTROPOMODEL = 1434,
   SERIALPROTOCOL = 1444,
   RTKSOURCETIMEOUT = 1445,
   RTKMATCHEDTIMEOUT = 1447,
   PSRDIFFSOURCETIMEOUT = 1449,
   PSRDIFFTIMEOUT = 1450,
   STEADYLINE = 1452,
   RAWIMUSX = 1462,
   INSOFFSETS = 1467,
   LEDCONFIG = 1498,
   DATADECODESIGNAL = 1532,
   PPPPOS = 1538,
   PPPRESET = 1542,
   PPPSEED = 1544,
   PPPDYNAMICS = 1551,
   PPPTIMEOUT = 1560,
   PPPCONVERGEDCRITERIA = 1566,
   NMEAVERSION = 1574,
   IPSERVICE = 1575,
   SETADMINPASSWORD = 1579,
   SETFILECOPYMODE = 1581,
   BDSECUTOFF = 1582,
   BLUETOOTHCONFIG = 1609,
   WIFICLICONFIG = 1614,
   WIFICLICONTROL = 1615,
   WIFICONFIG = 1617,
   NOVATELXOBS = 1618,
   NOVATELXREF = 1620,
   UALCONTROL = 1627,
   EVENTOUTCONTROL = 1636,
   EVENTINCONTROL = 1637,
   DUALANTENNAPOWER = 1639,
   IOCONFIG = 1663,
   WIFIAPCONFIG = 1665,
   LOGIN = 1671,
   LOGOUT = 1672,
   AIRPLANEMODE = 1674,
   BESTVELTYPE = 1678,
   SAVEETHERNETDATA = 1679,
   CELLULARCONFIG = 1683,
   SETPREFERREDNETIF = 1688,
   BLUETOOTHDISCOVERABILITY = 1690,
   PPPSOURCE = 1707,
   DHCPCONFIG = 1710,
   LBANDBEAMTABLE = 1718,
   TERRASTARINFO = 1719,
   TERRASTARSTATUS = 1729,
   ASSIGNLBANDBEAM = 1733,
   ELEVATIONCUTOFF = 1735,
   PPSCONTROL2 = 1740,
   TERRASTARPAYG = 1749,
   GLIDEINITIALIZATIONPERIOD = 1760,
   DUALANTENNAALIGN = 1761,
   GLIDERECOVERY = 1853,
   NMEAFORMAT = 1861,
   RADARCONFIG = 1878,
   INSCALIBRATE = 1882,
   INSSEED = 1906,
   SETINSTRANSLATION = 1920,
   SETINSROTATION = 1921,
   RTKPORTMODE = 1936,
   INSCONFIG = 1945,
   INSROVERPOSUPDATE = 1954,
   INSCALSTATUS = 1961,
   RTKASSIST = 1985,
   RTKASSISTTIMEOUT = 2003,
   SRTKSETKEY = 2004,
   SRTKSUBSCRIPTION = 2017,
   SRTKSUBSCRIPTIONS = 2018,
   GENERATESRTKCORRECTIONS = 2019,
   RTKASSISTSTATUS = 2048,
   CONFIGCRC = 8192,
   CGRVERSIONS = 8193,
   MM2TCONFIG = 8201,
   MM2TCONFIGN = 8202,
   MM2TSTATUS = 8203,
   MM2TINFO = 8204,
   P400CONFIG = 8206,
   P400STATUS = 8207,
   P400INFO = 8208,
   MM2TCONFIGL = 8209,
   CGRPDPDIAG = 8210,
   CGRRTKCORRDIAG = 8211,
   CGRALIGNCORRDIAG = 8212,
   CGRTRACKDIAG = 8213,
   CGRHWSTATUS = 8216
}

/**
* Response codes for receiver commands
**/
enum ResponseCode {
   UNKNOWN = 0,
   OK = 1, // MESSAGE WAS RECEIVED CORRECTLY
   REQUESTED_LOG_DOES_NOT_EXIST = 2, // THE LOG REQUESTED DOES NOT EXIST
   NOT_ENOUGH_RESOURCES_IN_SYSTEM = 3, // THE REQUEST HAS EXCEEDED A LIMIT
   DATA_PACKET_DOES_NOT_VERIFY = 4, // DATA PACKET IS NOT VERIFIED
   COMMAND_FAILED_ON_RECEIVER = 5, // COMMAND DID NOT SUCCEED IN ACCOMPLISHING REQUESTED TASK
   INVALID_MESSAGE_ID = 6, // THE INPUT MESSAGE ID IS NOT VALID
   INVALID_MESSAGE_FIELD = 7, // FIELD OF THE INPUT MESSAGE IS NOT CORRECT
   INVALID_CHECKSUM = 8, // THE CHECKSUM OF THE INPUT MESSAGE IS NOT CORRECT. ONLY APPLIES TO ASCII AND BINARY FORMAT MESSAGES.
   MESSAGE_MISSING_FIELD = 9, // A FIELD IS MISSING FROM THE INPUT MESSAGE
   ARRAY_SIZE_FOR_FIELD_EXCEEDS_MAX = 10, // FIELD CONTAINS MORE ARRAY ELEMENTS THAN ALLOWED
   PARAMETER_IS_OUT_OF_RANGE = 11, // FIELD OF THE INPUT MESSAGE IS OUTSIDE THE ACCEPTABLE LIMITS
   TRIGGER_NOT_VALID_FOR_THIS_LOG = 14, // TRIGGER TYPE IS NOT VALID FOR THIS TYPE OF LOG
   AUTHCODE_TABLE_FULL = 15, // TOO MANY AUTHCODES ARE STORED IN THE RECEIVER. THE RECEIVER FIRMWARE MUST BE RELOADED
   INVALID_DATE_FORMAT = 16, // THIS ERROR IS RELATED TO THE INPUTTING OF AUTHCODES. INDICATES THE DATE ATTACHED TO THE CODE IS NOT VALID
   INVALID_AUTHCODE_ENTERED = 17, // THE AUTHCODE ENTERED IS NOT VALID
   NO_MATCHING_MODEL_TO_REMOVE = 18, // THE MODEL REQUESTED FOR REMOVAL DOES NOT EXIST
   NOT_VALID_AUTHCODE_FOR_THAT_MODEL = 19, // THE MODEL ATTACHED TO THE AUTHCODE IS NOT VALID
   CHANNEL_IS_INVALID = 20, // THE SELECTED CHANNEL IS INVALID
   REQUESTED_RATE_IS_INVALID = 21, // THE REQUESTED RATE IS INVALID
   WORD_HAS_NO_MASK_FOR_THIS_TYPE = 22, // THE WORD HAS NO MASK FOR THIS TYPE OF LOG
   CHANNELS_LOCKED_DUE_TO_ERROR = 23, // CHANNELS ARE LOCKED DUE TO ERROR
   INJECTED_TIME_INVALID = 24, // INJECTED TIME IS INVALID
   COM_PORT_NOT_SUPPORTED = 25, // THE COM OR USB PORT IS NOT SUPPORTED
   MESSAGE_IS_INCORRECT = 26, // THE MESSAGE IS INVALID
   INVALID_PRN = 27, // THE PRN IS INVALID
   PRN_NOT_LOCKED_OUT = 28, // THE PRN IS NOT LOCKED OUT
   PRN_LOCKOUT_LIST_FULL = 29, // THE PRN LOCKOUT LIST IS FULL
   PRN_ALREADY_LOCKED_OUT = 30, // THE PRN IS ALREADY LOCKED OUT
   MESSAGE_TIMED_OUT = 31, // MESSAGE TIMED OUT
   UNKNOWN_COM_PORT_REQUESTED = 33, // UNKNOWN COM OR USB OR CAN PORT REQUESTED
   HEX_STRING_NOT_FORMATTED_CORRECTLY = 34, // HEX STRING NOT FORMATTED CORRECTLY
   INVALID_BAUD_RATE = 35, // THE BAUD RATE IS INVALID
   MESSAGE_IS_INVALID_FOR_THIS_MODEL = 36, // MESSAGE IS INVALID FOR THIS MODEL OF RECEIVER
   COMMAND_ONLY_VALID_IF_IN_NVM_FAIL_MODE = 40, // COMMAND IS ONLY VALID IF NVM IS IN FAIL MODE
   INVALID_OFFSET = 41, // THE OFFSET IS INVALID
   MAXIMUM_NUMBER_OF_USER_MESSAGES_REACHED = 78, // MAXIMUM NUMBER OF USER MESSAGES HAS BEEN REACHED
   GPS_PRECISE_TIME_IS_ALREADY_KNOWN = 84, // GPS PRECISE TIME IS ALREADY KNOWN
   ZUPT_DISABLED_BY_USER = 149, // AN INSZUPT COMMAND WAS SENT AFTER INSZUPTCONTROL COMMAND WAS USED TO DISABLE THE USE OF ZUPTS.
   IMU_SPECS_LOCKED_FOR_THIS_IMU_TYPE = 150, // SPAN ALLOWS THE DEFAULT SPECIFICATIONS FOR A SELECT FEW IMUS TO BE MODIFIED TO SUPPORT DIFFERENT VARIANTS. HOWEVER, MOST IMU SPECIFICATIONS ARE NOT ALLOWED TO CHANGE.
   COMMAND_INVALID_FOR_THIS_IMU = 154, // THE ENTERED COMMAND CANNOT BE USED WITH THE CONFIGURED IMU.   FOR EXAMPLE, THE LEVERARMCALIBRATE COMMAND IS NOT VALID FOR LOWER QUALITY IMUS.
   IMU_TYPE_IS_NOT_SUPPORTED_WITH_CURRENT_MODEL = 157, // A FIRMWARE MODEL UPGRADE IS REQUIRED TO USE THE REQUESTED IMU (SETIMUTYPE OR CONNECTIMU COMMAND).
   TRIGGER_BUFFER_IS_FULL = 161, // THE TIMEDEVENTPULSE LIMIT OF 10 EVENTS HAS BEEN REACHED, AND A NEW EVENT CANNOT BE SET UNTIL AN EVENT IS CLEARED.
   SETUPSENSOR_COMMAND_IS_LOCKED = 163, // THE SETUPSENSOR COMMAND CANNOT BE MODIFIED BECAUSE THERE ARE REMAINING TRIGGER EVENTS QUEUED.
}

/**
* Components in the Version log
*/
enum ComponentType {
   UNKNOWN = 0,
   GPS_CARD = 1,
   CONTROLLER = 2,
   ENCLOSURE = 3,
   OMNISTAR_CAN_INTERFACE1 = 4,
   OMNISTAR_CAN_INTERFACE2 = 5,
   OMNISTAR_CAN_INTERFACE3 = 6,
   IMU_CARD = 7,
   USERINFO = 8,
   OEM6FPGA = 12,
   GPSCARD2 = 13,
   BLUETOOTH = 14,
   WIFI = 15,
   CELLULAR = 16,
   RADIO = 18,
   OEM7FPGA = 21,
   APPLICATION = 22,
   PACKAGE = 23,
   DEFAULT_CONFIG = 25,
   DB_HEIGHTMODEL = 981073920,
   DB_USERAPP = 981073921,
   DB_USERAPPAUTO = 981073925,
}

/**
* Sbas system types
**/
enum SbasSystem {
   NONE = 0,
   AUTO = 1,
   ANY = 2,
   WAAS = 3,
   EGNOS = 4,
   MSAS = 5,
   GAGAN = 6,
   QZSS = 7,
   DISABLED = 99,
}

/**
* SBAS Switch
*/
enum SbasSwitch {
   DISABLE = 0,
   ENABLE = 1,
}

/**
* Sbas test mode
*
*/
enum SbasTestMode {
   NONE = 0,
   ZERO_TO_TWO = 1,
   IGNORE_ZERO = 2,
}

/**
 * Enums used for the LOG command and LOG responses
 */

/**
* Log removable by unlogall
**/
enum LogHold {
   NO_HOLD = 0, //Allow log to be removed by the UNLOGALL command
   HOLD = 1     //Prevent log from being removed by the default UNLOGALL command
}

/**
* When is the log triggered in receiver
**/
enum LogTrigger {
   ON_NEW = 0,
   ON_CHANGED = 1,
   ON_TIME = 2,
   ON_NEXT = 3,
   ONCE = 4,
   ON_MARK = 5
}

/**
* Receiver communication ports
**/
enum ReceiverPort {
   NO_PORT = 0,
   COM1 = 1,
   COM2 = 2,
   COM3 = 3,
   THIS_PORT = 6,
   FILE = 7,
   ALL_PORTS = 8,
   XCOM1 = 9,
   XCOM2 = 10,
   USB1 = 13,
   USB2 = 14,
   USB3 = 15,
   XCOM3 = 17,
   COM4 = 19,
   IMU = 21
   ICOM1 = 23
   ICOM2 = 24
   ICOM3 = 25
   NCOM1 = 26
   NCOM2 = 27
   NCOM3 = 28
   WCOM1 = 30
   COM5 = 31
   COM6 = 32
   BT1 = 33
   COM7 = 34
   COM8 = 35
   COM9 = 36
   COM10 = 37
   CCOM1 = 7840, //Can virtual comport 1
   CCOM2 = 8096, //Can virtual comport 2
   CCOM3 = 8352, //Can virtual comport 3
   CCOM4 = 8608, //Can virtual comport 4
   CCOM5 = 8864, //Can virtual comport 5
   CCOM6 = 9120  //Can virtual comport 6
}

/**
* Terrastar Enums
**/

/**
* Convergence Criteria
*/
enum PppConvergenceCriteria {
   TOTAL_STDDEV = 1,
   HORIZONTAL_STDDEV = 2
}

/**
* PPP Source type
*/
enum PppSourceType {
   NONE = 0,
   TERRASTAR = 1,
   VERIPOS = 2,
   PACE = 3,
   TERRASTAR_D = 6,
   TERRASTAR_L = 8,
   AUTO = 100
}

/**
* Subscription types
*/
enum TerrastarSubscriptionType {
   UNASSIGNED = 0,
   TERM = 1,
   PAYG = 2,
   BUBBLE = 100,
   MODEL_DENIED = 101
}

/**
* Region restrictions of the active subscription
*/
enum TerrastarRegionRestriction {
   NONE = 0,
   GEOGATED = 1,
   LOCAL_AREA = 2,
   NEAR_SHORE = 3
}

/**
* Access status. ENABLE (1) if the subscription is valid; DISABLE (0) otherwise
*/
enum TerrastarAccessStatus {
   DISABLED = 0,
   ENABLED = 1
}

/**
* Geogating status
*/
enum TerrastarGeoGatingStatus {
   DISABLED = 0,
   WAITING_FOR_POSITION = 1,
   ON_SHORE = 129,
   OFF_SHORE = 130,
   POSITION_TOO_OLD = 255,
   PROCESSING = 1000
}

/**
* For local-area subscriptions, indicates if the receiver is within the permitted area
*/
enum TerrastarLocalAreaStatus {
   DISABLED = 0,
   WAITING_FOR_POSITION = 1,
   RANGE_CHECK = 16,
   IN_RANGE = 129,
   OUT_OF_RANGE = 130,
   POSITION_TOO_OLD = 255
}

/**
* Decoder data synchronization state
*/
enum TerrastarSyncStatus {
   NO_SIGNAL = 0,
   SEARCH = 1,
   LOCKED = 2
}

/**
* Enums for Time log
**/

/**
* Clock Model Status
**/
enum ClockModelStatus {
   VALID = 0,
   CONVERGING = 1,
   ITERATING = 2,
   INVALID = 3,
   ERROR = 4
}

/**
* UTC Status
**/
enum UtcStatus {
   INVALID = 0,
   VALID = 1,
   WARNING = 2
}

/**
* SteadyLineMode
**/
enum SteadyLineMode {
   DISABLE = 0,
   MAINTAIN = 1,
   TRANSITION = 2,
   RESET = 3,
   PREFER_ACCURACY = 4,
   UAL = 5
}

/**
* UalControlAction
**/
enum UalControlAction {
   DISABLE = 0,
   ENABLE = 1,
   CLEAR = 2
}

/**
* Ins Calibration trigger
**/
enum InsCalibrationTrigger {
   STOP = 0,
   NEW = 1,
   ADD = 2,
   RESET = 3
}

/**
* Ins offset types
**/
enum InsOffsetType {
   UNKNOWN = 0,
   ANT1 = 1,
   ANT2 = 2,
   EXTERNAL = 3,
   USER = 4,
   MARK1 = 5,
   MARK2 = 6,
   GIMBAL = 7,
   ALIGN = 8,
   MARK3 = 9,
   MARK4 = 10,
   RBV = 11,
   RBM = 12,
   ENCLOSURE = 13
}

/**
* Input frame type
* Coordinate frame offset values are represented in
**/
enum InsCalibrationInputFrameType {
   IMUBODY = 0,
   VEHICLE = 1
}

/**
* Source from which offset values originate
**/
enum InsCalibrationSourceStatus {
   UNKNOWN = 0,
   FROM_NVM = 1,
   CALIBRATING = 2,
   CALIBRATED = 3,
   FROM_COMMAND = 4,
   RESET = 5,
   FROM_DUAL_ANT = 6,
   INS_CONVERGING = 7,
   INSUFFICIENT_SPEED = 8,
   HIGH_ROTATION = 9
}

/**
* Interface mode of a given port
**/
enum PortInterfaceMode {
   NONE = 0,
   NOVATEL = 1,
   RTCM = 2,
   RTCA = 3,
   CMR = 4,
   OMNISTAR = 5,
   RTCMNOCR = 8,
   TCOM1 = 10,
   TCOM2 = 11,
   TCOM3 = 12,
   TAUX = 13,
   RTCMV3 = 14,
   NOVATELBINARY = 15,
   GENERIC = 18,
   MRTCA = 20,
   AUTO = 27,
   NOVATELX = 35,
   NOVATELMINBINARY = 49,
   NOVATELMULTI = 57
}

/**
* Reason for last PDP initialization
**/
enum PDPResetCause {
   INIT = 1,
   SIGNAL_OUTAGE = 2,
   USER_REQUESTED = 3
}

/**
* Correction format used in diagnostic logs
**/
enum CorrectionFormat {
   NONE = 0,
   RTCM = 1,
   RTCA = 2,
   CMR = 3,
   RTCMV3 = 4,
   NOVATELX = 5,
   RTCA_ALIGN = 6,
   NOVATELX_ALIGN = 7
}

/**
* Satellite system used in diagnostic logs
**/
enum CorrectionSatelliteSystem {
   GPS = 0,
   GLONASS = 1,
   GALILEO = 2,
   BEIDOU = 3
}

/**
* Frequencies used in diagnostic logs
**/
enum CorrectionSatelliteFrequency {
   L1 = 0, //L1 if GPS/GLONASS, E1 if Galileo, B1 if BeiDou
   L2 = 1, //L2 if GPS/GLONASS, E5 if Galileo, B2 if BeiDou
   L5 = 2  //L5 (GPS only)
}

/**
* Options for INSCOMMAND message
**/
enum InsCommandOption {
   RESET = 0
}

/**
* Battery Input Voltage Failure mode indicators
**/
enum BatteryInputVoltageFailureIndicator {
   VOLTAGE_HIGH_MOST_SEVERE = 0,
   VOLTAGE_LOW_MOST_SEVERE = 1,
   VOLTAGE_HIGH_SHORTED = 3,
   VOLTAGE_LOG_SHORTED = 4,
   VOLTAGE_HIGH_MODERATELY_SEVERE = 16
   OK = 31
}

/**
* Battery Input Voltage Failure mode indicators
**/
enum Oem7TemperatureFailureIndicator {
   TEMPERATURE_UPPER_ERROR_LIMIT = 0,
   TEMPERATURE_LOWER_ERROR_LIMIT = 1,
   TEMPERATURE_UPPER_WARNING_LIMIT = 16
   TEMPERATURE_LOWER_WARNING_LIMIT = 17,
   OK = 31
}

/**
* Battery Input Voltage Failure mode indicators
**/
enum AntennaFailureIndicator {
   UNKNOWN = 0,
   ANTENNA_OPEN = 5,
   ANTENNA_SHORTENED = 6,
   ANTENNA_POWER_WARNING = 16,
   OK = 31,
}

/**
*  Action to perform on subscription.
*  used in SRTKSUBSCRIPTION command
**/
enum SecureRTKSubscriptionAction {
   SET = 0,
   REMOVE = 1,
   ACTIVATE = 2,
   DEACTIVATE = 3
}

/**
* Current state of secure RTK subscription
* used in SRTKSUBSCRIPTIONS command
**/
enum SecureRTKSubscriptionState {
   INACTIVE = 0,
   EXPIRED = 1,
   ACTIVE = 2
}

/**
* Secure RTK subscription status
* used in SRTKSUBSCRIPTIONS command
**/
enum SecureRTKSubscriptionStatus {
   NONE = 0,
   OKAY = 1,
   SELFTEST_FAILED = 2,
   WRONG_KEY_LENGTH = 3,
   WEAK_KEY = 4,
   WRONG_LENGTH = 5,
   BAD_KEY = 6,
   MESSAGE_TOO_BIG = 7,
   EXPIRED_KEY = 8,
   KEY_TO_BE_VERIFIED = 9
}

/**
* DGPS Type, used in RTKSOURCE, PSRDIFFSOURCE
**/
enum DGPSType {
   RTCM = 0,
   RTCA = 1,
   CMR = 2,
   OMNISTAR = 3,
   SBAS = 5,
   RTK = 6,
   AUTO = 10,
   NONE = 11,
   RTCMV3 = 13,
   NOVATELX = 14
}

/**
* RTKCommand actions
**/
enum RTKCommandAction {
   USE_DEFAULTS = 0,
   RESET = 1
}

/**
* Dynamics mode
**/
enum DynamicsMode {
   AUTO = 0,
   STATIC = 1,
   DYNAMIC = 2
}

/**
* General enabled/disabled enum
* used in multiple commands/logs
**/
enum EnabledDisabled {
   DISABLED = 0,
   ENABLED = 1
}

/**
* RTCM versions, only need to be set when using RTCM v2.x
**/
enum RtcmVersion {
   RTCM_V23 = 0,    //RTCM version 2.3
   RTCM_V22 = 1     //RTCM version 2.2
}

/**
* Options for PPPSEED command
**/
enum PPPSeedOption {
   CLEAR = 0,
   SET = 1,
   STORE = 2,
   RESTORE = 3,
   AUTO = 4
}

/**
* InsSeedingOption
**/
enum InsSeedingOption {
   DISABLE = 0,
   ENABLE = 1,
   CLEAR = 2
}

/**
* Internal radio state, used in P400Status and MM2TStatus logs
**/
enum InternalRadioState {
   UNAVAILABLE = 0,
   OFF = 1,
   INITIALIZING = 2,
   PROGRAMMING = 3,
   ONLINE = 4,
   OFFLINE = 5,
   UPGRADE = 6,
   FAILURE = 7
}

/**
* Rtk assist (rtk bridge) state
**/
enum RtkAssistState {
   INACTIVE = 0,
   ACTIVE = 1
}

/**
* Rtk assist (rtk bridge) mode
**/
enum RtkAssistMode {
   UNAVAILABLE = 0,
   COAST = 1,
   ASSIST = 2
}

/**
* Limit type used in rtkassisttimeout command/log
**/
enum RtkAssistLimitType {
   SUBSCRIPTION_LIMIT= 0,
   USER_LIMIT = 1
}

/**
* Nmea field to format
**/
enum NmeaFormatField {
   GGA_LATITUDE = 0,
   GGA_LONGITUDE = 1,
   GGA_ALTITUDE = 2,
   GGA_UNDULATION = 3,
   GGA_AGE = 4
}

/**
* Status of the Firmware Signature
**/
enum AuthCodeSignatureStatus {
   UNKNOWN = 0,
   NONE = 1
   INVALID = 2,
   VALID = 3,
   RESERVED = 4,
   HIGH_SPEED = 5
}

/**
* Auth code type
**/
enum AuthCodeType {
   UNKNOWN = 0,
   STANDARD = 1,
   SIGNATURE = 2
}

/**
* Auth code function to perform
**/
enum AuthCommand {
   REMOVE = 0,
   ADD = 1,
   ADD_DOWNLOAD = 4,
   ERASE_TABLE= 7,
   CLEAN_TABLE = 8
}